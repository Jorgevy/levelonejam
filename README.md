# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repository created for having version control during the LevelOneJam 2019 at ITU
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Create your own branch from "development" when you want to work on something. Always remember to pull from development before
* doing so.
* Push to your own branch every 15 minutes to make sure that you don't loose progress in case of computer malfunction. Always
* comment in te commit the small changes you may have made. 
* Whenever you have finished working on the future you where hands-in in your branch, please, pull again from development, 
* push your changes to the branch and close it.
* Repeat the above mentioned steps anytime you want to create a new feature. This will help us maintain a proper version control
* and will help us enourmously fixing bugs and other things of the like.

### Who do I talk to? ###

* If you have any doubt just come ask me any time! (Jorge)